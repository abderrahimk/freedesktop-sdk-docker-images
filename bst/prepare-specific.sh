set -o errexit

dnf install -y gpg git rsync
dnf install -y --enablerepo=updates-testing libabigail flatpak flatpak-builder
